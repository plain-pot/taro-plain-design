export default {
  pages: [
    'pages/index/index',
    // 'pages/components/list',
    // 'pages/components/loading',
    // 'pages/components/icon',
    'pages/components/button',
  ],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black'
  }
}
