import './index.scss'
import {designPage} from 'plain-design-composition';
import {PlButton, PlIcon, PlItem, PlList, PlListTitle, PlPage} from "../../packages";
import {iMenu, menus} from "./menus";
import React from 'react';
import {navigateTo} from '@tarojs/taro';

export default designPage(() => {

  const openMenu = (menu: iMenu) => {
    console.log(menu)
    navigateTo({url: menu.path})
  }

  return () => (
    <PlPage>
      {menus.map(group => <React.Fragment key={group.name}>
        <PlListTitle label={group.name}/>
        <PlButton label={group.name} onClick={() => openMenu(group.name as any)}/>
        <PlList>
          {group.menus.map(menu => (
            <PlItem key={menu.name} label={menu.name} showArrow onClick={() => openMenu(menu)}>
              {{
                contentSlot: () => !!menu.done && <PlIcon icon="ios-checkmark"/>
              }}
            </PlItem>
          ))}
        </PlList>
      </React.Fragment>)}
    </PlPage>
  )
})
