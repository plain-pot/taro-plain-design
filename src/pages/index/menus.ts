export interface iMenu {
  name: string,
  path: string,
  done?: boolean,
}

interface iMenuGroup {
  name: string,
  menus: iMenu[],
}

export const menus: iMenuGroup[] = [
  {
    name: '基础组件',
    menus: [
      {name: 'Button 按钮', path: '/pages/components/button'},
      {name: 'Icon 图标', path: '/pages/components/icon'},
      {name: 'Loading 加载中', path: '/pages/components/loading'},
    ],
  },
  {
    name: '表单组件',
    menus: [
      {name: 'Input 文本', path: '/pages/components/input'},
      {name: 'Number 数字', path: '/pages/components/number'},
      {name: 'Image 图片', path: '/pages/components/image'},
      {name: 'Date 日期', path: '/pages/components/date'},
      {name: 'Select 选择', path: '/pages/components/select'},
      {name: 'Address 地址选择', path: '/pages/components/address'},
      {name: 'Cascade 级联选择', path: '/pages/components/cascade'},
    ],
  },
  {
    name: '页面布局',
    menus: [
      {name: 'List 列表', path: '/pages/components/list', done: true},
      {name: 'Grid 布局', path: '/components/grid'},
      {name: 'SwipeAction 侧滑', path: '/components/swipe-action'},
      {name: 'Skeleton 骨架屏', path: '/components/skeleton'},
      {name: 'Step 步骤条', path: '/components/step'},
      {name: 'Carousel 轮播', path: '/components/carousel'},
      {name: 'Tab 页签', path: '/components/tab'},
      {name: 'Tabbar 标签栏', path: '/components/tabbar'},
    ]
  },
]
