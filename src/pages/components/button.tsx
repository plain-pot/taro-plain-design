import {designPage} from "plain-design-composition";
import {View} from "@tarojs/components";
import {PlButton} from "../../packages";
import { navigateBack } from "@tarojs/taro";

export default designPage(() => {
  return () => (
    <View>
      this is button page
      <PlButton onClick={() => navigateBack()}>
        BACK
      </PlButton>
    </View>
  )
})
