import './icon.scss'
import './iconfont/iconfont.css'
import {designComponent, useClasses} from "plain-design-composition";
import allIcons from './icon.json'
import {View} from "../../utils/TaroComponents";

export const PlIcon = designComponent({
  name: 'pl-icon',
  expose: {
    allIcons,
  },
  props: {
    icon: {type: String},
    status: {type: String}
  },
  setup({props}) {
    const classes = useClasses(() => [
      'pl-icon', props.icon, 'iconfont',
      {[`pl-color-${props.status}`]: !!props.status,}
    ])
    return () => <View className={classes.value}/>
  },
})

export default PlIcon
