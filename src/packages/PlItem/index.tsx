import {computed, designComponent, iHTMLDivElement, useClasses} from "plain-design-composition";
import {View} from "../../utils/TaroComponents";
import PlIcon from "../PlIcon";
import PlList from "../PlList";
import {BaseTouchEvent, ITouch} from "@tarojs/components";

export const PlItem = designComponent({
  inheritPropsType: iHTMLDivElement,
  name: 'pl-item',
  props: {
    label: {type: [String, Number]},
    content: {type: [String, Number]},
    desc: {type: [String, Number]},
    note: {type: [String, Number]},
    icon: {type: String},
    showArrow: {type: Boolean},
    data: {},
  },
  slots: [
    'labelSlot',
    'contentSlot',
    'default',
    'descSlot',
    'noteSlot',
    'thumbSlot',
    'iconSlot',
  ],
  emits: {
    onClick: (e: BaseTouchEvent<ITouch>) => true,
  },
  setup({props, slots, event: {emit}}) {

    const list = PlList.use.inject(null)

    const icon = computed(() => {
      if (!!props.icon) {return props.icon}
      if (props.showArrow) {return 'ios-arrow-forward'}
      return null
    })

    const handler = {
      onClick: (e: BaseTouchEvent<ITouch>) => {
        console.log('tap', e)
        emit.onClick(e)
        if (!!list) {
          list.handler.onClickItem(e, props.data as any)
        }
      }
    }

    const classes = useClasses(() => [
      'pl-item', {'pl-item-clickable': !!icon.value}
    ])

    return () => {
      return (
        <View className={classes.value} onClick={handler.onClick} hover-class="pl-hover">
          {slots.thumbSlot.isExist() && (
            <View className="pl-item-thumb">{slots.thumbSlot()}</View>
          )}
          <View className="pl-item-body">
            {(props.label || props.content || slots.labelSlot.isExist() || slots.contentSlot.isExist() || slots.default.isExist()) && (
              <View className="pl-item-label-line">
                {(props.label || slots.labelSlot.isExist()) && (
                  <View className="pl-item-label">{slots.labelSlot(props.label)}</View>
                )}
                {(props.content || slots.contentSlot.isExist() || slots.default.isExist()) && (
                  <View className="pl-item-content">{slots.default(slots.contentSlot(props.content))}</View>
                )}
              </View>
            )}
            {(props.note || props.desc || slots.noteSlot.isExist() || slots.descSlot.isExist()) && (
              <View className="pl-item-note-line">
                {(props.note || slots.noteSlot.isExist()) && (
                  <View className="pl-item-note">{slots.noteSlot(props.note)}</View>
                )}
                {(props.desc || slots.descSlot.isExist()) && (
                  <View className="pl-item-desc">{slots.descSlot(props.desc)}</View>
                )}
              </View>
            )}
          </View>
          {(!!icon.value || slots.iconSlot.isExist()) && (
            slots.iconSlot(<PlIcon icon={icon.value!}/>)
          )}
        </View>
      )
    }
  },
})

export default PlItem
