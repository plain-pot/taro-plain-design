import {designComponent, onMounted} from "plain-design-composition";
import {setNavigationBarTitle} from '@tarojs/taro'
import {View} from '../../utils/TaroComponents'
import './page.scss'

export const PlPage = designComponent({
  name: 'pl-page',
  props: {
    theme: {type: Object},
    navigationBarTitle: {type: String},
  },
  slots: [
    'default'
  ],
  setup({slots, props}) {

    if (!!props.navigationBarTitle) {
      onMounted(() => {
        setNavigationBarTitle({title: props.navigationBarTitle!})
      })
    }

    return () => (
      <View className="pl-page">
        {slots.default()}
      </View>
    )
  },
})

export default PlPage

