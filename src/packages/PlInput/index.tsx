import {designComponent, useModel} from 'plain-design-composition'
import {Input, View} from '../../utils/TaroComponents'

export const PlInput = designComponent({
  name: 'pl-input',
  props: {
    modelValue: {},
  },
  emits: {
    onUpdateModelValue: (val?: any) => true,
  },
  setup({props, event: {emit}}) {
    const model = useModel(() => props.modelValue, emit.onUpdateModelValue)
    const handler = {
      onChange: (e: any) => {
        model.value = e.target.value
      }
    }
    return () => (
      <View>
        input before
        <Input type="text" v-model={model.value}/>
        input after
      </View>
    )
  },
})

export default PlInput
