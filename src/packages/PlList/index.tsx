import {designComponent} from "plain-design-composition";
import {View} from "../../utils/TaroComponents";
import './list.scss'

export const PlList = designComponent({
  name: 'pl-list',
  provideRefer: true,
  props: {},
  slots: ['default'],
  emits: {
    onClickItem: (data: any, e: Event) => true,
  },
  setup({props, slots, event: {emit}}) {

    const handler = {
      onClickItem: (data: any, e: Event) => {
        emit.onClickItem(data, e)
      }
    }

    return {
      refer: {
        handler
      },
      render: () => (
        <View className="pl-list">
          {slots.default()}
        </View>
      ),
    }
  },
})

export default PlList
