import './select.scss'
import {designComponent, reactive} from 'plain-design-composition'
import {View} from '../../utils/TaroComponents'

const PlSelect = designComponent({
  name: 'pl-select',
  setup() {

    const state = reactive({
      val: null as any,
    })

    const options = [
      '张三',
      '李四',
      '王五'
    ]

    const handler = {
      onChange: (e: any) => {
        state.val = options[e.detail.value] as any
        console.log(state.val)
      }
    }

    return () => (
      <View>
        当前选中选项：{state.val}
      </View>
    )
  },
})

export default PlSelect
