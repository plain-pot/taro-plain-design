import {computed, designComponent, PropType, useClasses} from "plain-design-composition";
import './button.scss'
import PlIcon from "../PlIcon";
import {EditProps, useEdit} from "../../use/useEdit";
import PlLoading from "../PlLoading";
import {Button} from "../../utils/TaroComponents";
import {BaseTouchEvent, ITouch} from "@tarojs/components";

console.log('load pl-button', {Button})

export const PlButton = designComponent({
  inheritPropsType: {} as typeof HTMLButtonElement,
  name: 'pl-button',
  slots: ['default'],
  props: {
    label: {type: String},        // 按钮文本
    icon: {type: String},         // 按钮图标
    status: {type: String},       // 按钮状态: primary,success,warn,error
    mode: {type: String},         // 按钮模式: fill,stroke,flat,text
    size: {type: String},         // 按钮大小: sm,md,lg,xl
    block: {type: Boolean},       // 块级按钮
    asyncHandler: {type: Function as PropType<(e: BaseTouchEvent<ITouch>) => any>},// 异步处理函数，会自动开启loading以及关闭loading
    ...EditProps,
  },
  emits: {
    onClick: (e: BaseTouchEvent<ITouch>) => true,
  },
  setup({props, slots, event}) {

    const {editComputed, editState} = useEdit()

    const mode = computed(() => props.mode || 'stroke')

    const status = computed(() => {
      if (!!props.status) {return props.status}
      if (mode.value === 'fill' || mode.value === 'flat') {return 'primary'}
      return null
    })

    const iconOnly = computed(() => !props.label && !slots.default.isExist())

    const classes = useClasses(() => [
      'pl-button',
      `pl-button-mode-${mode.value}`,
      `pl-button-size-${props.size || 'lg'}`,
      {
        [`pl-button-status-${status.value}`]: !!status.value,
        'pl-button-icon-only': iconOnly.value,
        'pl-button-block': !!props.block,
        'pl-button-disabled': !!props.disabled,
      }
    ])

    const handler = {
      onClick: async (e: BaseTouchEvent<ITouch>) => {
        if (!editComputed.value.editable) {return}
        if (!!props.asyncHandler) {
          editState.loading = true
          try {
            await props.asyncHandler(e)
          } catch (e) {
            console.error(e)
          } finally {
            editState.loading = false
          }
        } else {
          event.emit.onClick(e)
        }
      }
    }

    return () => (
      <Button className={classes.value} hover-class="pl-button-hover" onClick={handler.onClick}>
        {!!editComputed.value.loading && <PlLoading/>}
        {slots.default(props.label)}
        {!!props.icon && !editComputed.value.loading && <PlIcon icon={props.icon}/>}
      </Button>
    )
  },
})

export default PlButton
