import {designComponent} from "plain-design-composition";

console.log('load pl-card')

export const PlCard = designComponent({
  inheritPropsType: {} as typeof HTMLButtonElement,
  name: 'pl-card',
  slots: ['default'],
  setup({props, slots}) {
    return () => (
      <view>
        this is card
      </view>
    )
  },
})

export default PlCard
