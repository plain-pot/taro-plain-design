import {designComponent} from "plain-design-composition";
import {View} from "../../utils/TaroComponents";
import './list-title.scss'

export const PlListTitle = designComponent({
  name: 'pl-list-title',
  props: {
    label: {type: String},
  },
  slots: ['default'],
  setup({props, slots}) {
    return () => (
      <View className="pl-list-title">
        {slots.default(props.label)}
      </View>
    )
  },
})

export default PlListTitle
