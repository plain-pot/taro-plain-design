import {designComponent, PropType, useClasses} from "plain-design-composition";
import {View} from "../../utils/TaroComponents";
import './loading.scss'

export const PlLoading = designComponent({
  name: 'pl-loading',
  props: {
    type: {type: String as PropType<'alpha' | 'beta' | 'gamma'>, default: 'alpha'},
    status: {type: String,},
    size: {type: String as PropType<'sm' | 'md' | 'lg' | 'xs'>},
  },
  setup({props}) {
    const classes = useClasses(() => [
      'pl-loading',
      `pl-loading-${props.type}`,
      {
        [`pl-color-${props.status}`]: !!props.status,
        [`pl-loading-size-${props.size}`]: !!props.size,
      }
    ])
    return () => (
      <View className={classes.value}>
        {types[props.type]}
      </View>
    )
  },
})

const types = {
  alpha: (
    <View className="pl-loading-tag">
      {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].map(i => (
        <View className="pl-loading-inner" key={i}/>
      ))}
    </View>
  ),
  beta: (
    <>{[0, 1, 2, 3, 4, 5, 6, 7, 8, 9].map(i => (
      <View className={`pl-loading-tag pl-loading-tag-${i + 1}`} key={i}>
        <View className={`pl-loading-inner pl-loading-inner-${i + 1}`}/>
      </View>
    ))}</>
  ),
  gamma: (
    <>{[0, 1, 2, 3, 4].map(i => (
      <View className={`pl-loading-tag pl-loading-tag-${i + 1}`}>
        <View className={`pl-loading-inner pl-loading-inner-${i + 1}`}/>
      </View>
    ))}</>
  )
}

export default PlLoading
