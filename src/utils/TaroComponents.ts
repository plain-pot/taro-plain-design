import {Button as button, Input as input, Text as text, View as view} from '@tarojs/components'

export const Button = button || 'button'
export const Input = input || 'input'
export const Text = text || 'text'
export const View = view || 'div'
