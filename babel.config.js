const JSXModel = require("plain-design-composition/plugins/babel-plugin-react-model.js")

// babel-preset-taro 更多选项和默认值：
// https://github.com/NervJS/taro/blob/next/packages/babel-preset-taro/README.md
module.exports = {
  presets: [
    ['taro', {
      framework: 'react',
      ts: true
    }]
  ],
  plugins: [
    JSXModel,
  ]
}
